import time
import asyncio
import traceback
import praw
import discord


# Discord configuration.
DISCORD_TOKEN = ''
DISCORD_CHANNEL_ID = ''
# Reddit credentials.
CLIENT_ID = ''
CLIENT_SECRET = ''
USER_AGENT = 'reddit/discord notifier bot (by /u/gourari)'
USERNAME = ''
PASSWORD = ''
SUBREDDIT_NAME = 'all'  # Without the prefix "r/".

KEYWORDS = ['key', 'keyword', 'keyword']  # Case insensitive.
CHECK_EVERY = 60  # In seconds.

async def main():
    await client.wait_until_ready()
    reddit = praw.Reddit(client_id=CLIENT_ID ,
                         client_secret=CLIENT_SECRET,
                         user_agent=USER_AGENT,
                         username=USERNAME,
                         password=PASSWORD)
    subreddit = reddit.subreddit(SUBREDDIT_NAME)
    channel = discord.Object(id=DISCORD_CHANNEL_ID)
    submissions = subreddit.stream.submissions(skip_existing=True,
                                               pause_after=0)
    comments = subreddit.stream.comments(skip_existing=True, pause_after=0)
    keywords_lower = [keyword.lower() for keyword in KEYWORDS]
    comment_link = 'https://www.reddit.com{}'
    print('Running...')
    while True:
        try:
            detected = []
            for submission in submissions:
                if submission is None:
                    break
                elif (any(keyword in submission.title.lower()
                          for keyword in keywords_lower) or
                      any(keyword in submission.selftext.lower()
                          for keyword in keywords_lower)):
                    detected.append('https://www.reddit.com' + submission.permalink)
            for comment in comments:
                if comment is None:
                    break
                elif any(keyword in comment.body.lower()
                         for keyword in keywords_lower):
                    detected.append(comment_link.format(comment.permalink))
            for link in detected:
                await client.send_message(channel, link)
            await asyncio.sleep(CHECK_EVERY)
        except Exception:
            traceback.print_exc()
            print('\n\n')

if __name__ == '__main__':
    client = discord.Client()
    client.loop.create_task(main())
    client.run(DISCORD_TOKEN)
