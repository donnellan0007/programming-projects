import random

"""

Very simple game of rock paper scissors. It has a built in AI, so that's something I guess

"""

__author__ = 'Seamus Donnellan | u/insane_playzYT'

def game(win_opt,opponent,player_1,option,):
    print(f"You chose to play against the {option}! Good choice!")
    option = input("Take your turn. Rock, paper or scissors? ")
    if option == 'Rock' or option == 'ROCK' or option == 'rock':
        opponent = random.choice(win_opt)
        print(f"The AI chose {opponent}")
        if opponent == 'Rock':
            return "The game is drawn"
        elif opponent == 'Paper':
            return "AI Wins"
        elif opponent == 'Scissors':
            return f"You win, {player_1}"
    elif option == 'Paper' or option == 'paper' or option == 'PAPER':
        opponent = random.choice(win_opt)
        print(f"The AI chose {opponent}")
        if opponent == 'Paper':
            return "The game is drawn"
        elif opponent == 'Rock':
            return f"You win, {player_1}"
        elif opponent == 'Scissors':
            return "AI Wins"
    elif option == 'Scissors' or option == 'scissors' or option == 'SCISSORS':
        opponent = random.choice(win_opt)
        print(f"The AI chose {opponent}")
        if opponent == 'Scissors':
            return "The game is drawn"
        elif opponent == 'Rock':
            return "AI Wins"
        elif opponent == 'Paper':
            return f"You win, {player_1}"


# this starts the game
play_time = int(input("How many times do you want to play? "))
while play_time <= play_time:
    print(game(['Rock','Paper','Scissors'],"",input("What is your name? "),"",))