import random

win = ""
opponent = ""
player_1 = input("What is your name? ")
option = input(f"Hello, {player_1}, and welcome to rock paper scissors. Do you want to verse a friend or an AI? AI or Friend: ")
win_opt = ['Rock','Paper','Scissors']

print(f"You chose to play against the {option}! Good choice!")

option = input("Take your turn. Rock, paper or scissors? ")
if option == 'Rock' or option == 'ROCK' or option == 'rock':
    opponent = random.choice(win_opt)
    print(f"The AI chose {opponent}")
    if opponent == 'Rock':
        print("The game is drawn")
    elif opponent == 'Paper':
        print("AI Wins")
    elif opponent == 'Scissors':
        print(f"You win, {player_1}")
elif option == 'Paper' or option == 'paper' or option == 'PAPER':
    opponent = random.choice(win_opt)
    print(f"The AI chose {opponent}")
    if opponent == 'Paper':
        print("The game is drawn")
    elif opponent == 'Rock':
        print(f"You win, {player_1}")
    elif opponent == 'Scissors':
        print("AI Wins")
elif option == 'Scissors' or option == 'scissors' or option == 'SCISSORS':
    opponent = random.choice(win_opt)
    print(f"The AI chose {opponent}")
    if opponent == 'Scissors':
        print("The game is drawn")
    elif opponent == 'Rock':
        print("AI Wins")
    elif opponent == 'Paper':
        print(f"You win, {player_1}")