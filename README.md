# python-projects
This is a repo for all my Python projects

#### I will keep all of the projects I make in Python in this repo

##### First "project" added: Word Counter. Super simple program that counts words.

##### Second Project added: Wordsearch solver. Whilst this isn't flashy by any means, it works. The user puts in the list of words, each spaced out by one letter, and then the words they have to find, each word with a space in between. It then checks to see whether or not any of those words are in the words.txt file, which is a file of over 56,000 English words. If there are connections, it will add a line of text saying "Found the word"

##### Third project added: Password manager. I made this whilst learning PyQt5 and QtDesigner. The UI is made in in QtDesigner and the functionality is in PyQt. I wouldn't recommend using this as your number one password manager though, because at the moment anyone can access it. I am thinking of adding a login system to it, but that's a bit down the road.

##### Fourth project added: Notepad. It is very simple and at the moment I am finding a way in PyQt5 to open files and read them. Reading the files is the easy bit, opening files is a bit harder.