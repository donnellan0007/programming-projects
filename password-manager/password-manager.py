# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '.\passwordmanager.ui'
#
# Created by: PyQt5 UI code generator 5.13.0
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets




class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(800, 600)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.columnView = QtWidgets.QColumnView(self.centralwidget)
        self.columnView.setGeometry(QtCore.QRect(10, 0, 781, 111))
        self.columnView.setAutoFillBackground(False)
        self.columnView.setObjectName("columnView")
        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setGeometry(QtCore.QRect(260, 10, 331, 71))
        font = QtGui.QFont()
        font.setFamily("Montserrat")
        font.setPointSize(25)
        font.setBold(True)
        font.setItalic(False)
        font.setWeight(75)
        self.label.setFont(font)
        self.label.setCursor(QtGui.QCursor(QtCore.Qt.ArrowCursor))
        self.label.setMouseTracking(False)
        self.label.setObjectName("label")
        self.label_2 = QtWidgets.QLabel(self.centralwidget)
        self.label_2.setGeometry(QtCore.QRect(340, 70, 171, 16))
        font = QtGui.QFont()
        font.setFamily("Montserrat")
        font.setBold(True)
        font.setItalic(True)
        font.setWeight(75)
        self.label_2.setFont(font)
        self.label_2.setObjectName("label_2")
        self.password_input = QtWidgets.QPlainTextEdit(self.centralwidget)
        self.password_input.setGeometry(QtCore.QRect(250, 240, 251, 31))
        font = QtGui.QFont()
        font.setPointSize(12)
        font.setBold(False)
        font.setWeight(50)
        self.password_input.setFont(font)
        self.password_input.setAutoFillBackground(False)
        self.password_input.setInputMethodHints(QtCore.Qt.ImhNone)
        self.password_input.setPlainText("")
        self.password_input.setPlaceholderText("")
        self.password_input.setObjectName("password_input")
        self.savebtn = QtWidgets.QPushButton(self.centralwidget)
        self.savebtn.clicked.connect(self.clicked)
        self.savebtn.setGeometry(QtCore.QRect(250, 280, 91, 31))
        font = QtGui.QFont()
        font.setFamily("Montserrat")
        self.savebtn.setFont(font)
        self.savebtn.setObjectName("savebtn")
        self.password_text = QtWidgets.QLabel(self.centralwidget)
        self.password_text.setGeometry(QtCore.QRect(250, 220, 47, 13))
        self.password_text.setObjectName("password_text")
        self.website_text = QtWidgets.QLabel(self.centralwidget)
        self.website_text.setGeometry(QtCore.QRect(250, 170, 81, 16))
        self.website_text.setObjectName("website_text")
        self.website_input = QtWidgets.QPlainTextEdit(self.centralwidget)
        self.website_input.setGeometry(QtCore.QRect(250, 190, 251, 31))
        font = QtGui.QFont()
        font.setPointSize(12)
        font.setBold(False)
        font.setWeight(50)
        self.website_input.setFont(font)
        self.website_input.setAutoFillBackground(False)
        self.website_input.setInputMethodHints(QtCore.Qt.ImhNone)
        self.website_input.setPlainText("")
        self.website_input.setPlaceholderText("")
        self.website_input.setObjectName("website_input")
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 800, 21))
        self.menubar.setObjectName("menubar")
        self.menuFile = QtWidgets.QMenu(self.menubar)
        self.menuFile.setObjectName("menuFile")
        self.menuEdit = QtWidgets.QMenu(self.menubar)
        self.menuEdit.setObjectName("menuEdit")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)
        self.actionNew_Windows = QtWidgets.QAction(MainWindow)
        self.actionNew_Windows.setObjectName("actionNew_Windows")
        self.actionFile_location = QtWidgets.QAction(MainWindow)
        self.actionFile_location.setObjectName("actionFile_location")
        self.menuFile.addAction(self.actionNew_Windows)
        self.menuEdit.addAction(self.actionFile_location)
        self.menubar.addAction(self.menuFile.menuAction())
        self.menubar.addAction(self.menuEdit.menuAction())

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def clicked(self):
        self.savebtn.setText("Saved!")

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.label.setText(_translate("MainWindow", "Password Manager"))
        self.label_2.setText(_translate("MainWindow", "Made by Seamus Donnellan"))
        self.savebtn.setText(_translate("MainWindow", "Save Password"))
        # self.savebtn.clicked.connect.setText(_translate("MainWindow", "Saved!")))
        self.password_text.setText(_translate("MainWindow", "Password"))
        self.website_text.setText(_translate("MainWindow", "Name of website"))
        self.menuFile.setTitle(_translate("MainWindow", "File"))
        self.menuEdit.setTitle(_translate("MainWindow", "Edit"))
        self.actionNew_Windows.setText(_translate("MainWindow", "New Windows"))
        self.actionFile_location.setText(_translate("MainWindow", "File location"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())
